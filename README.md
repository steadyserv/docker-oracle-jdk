# steadyserv/oracle-jdk Docker Images

Alpine-based docker images providing Oracle JDK.

For reliability, the JDK is downloaded from a SteadyServ S3 bucket.

Currently, only JDK 7 is available, and can be pulled with the `7` tag.
